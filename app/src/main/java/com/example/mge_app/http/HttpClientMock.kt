package com.example.mge_app.http

class HttpClientMock(private val bodyResponse: String) : HttpClient {
    override suspend fun get(url: String): String {
        return bodyResponse
    }

    companion object {
        val MockClient = HttpClientMock(getMockResponse())
    }
}

fun getMockResponse(): String {
    return """
        {
          "meta": {
            "code": 200,
            "disclaimer": "Usage subject to terms: https://currencybeacon.com/terms"
          },
          "response": {
            "date": "2023-10-04T15:54:08Z",
            "base": "USD",
            "rates": {
              "GBP": 0.82344155,
              "JPY": 148.86174965,
              "USD": 1,
              "CHF": 0.91683702
            }
          },
          "date": "2023-10-04T15:54:08Z",
          "base": "USD",
          "rates": {
            "GBP": 0.82344155,
            "JPY": 148.86174965,
            "USD": 1,
            "CHF": 0.91683702
          }
        }
    """.trimIndent()
}
