package com.example.mge_app.http

interface HttpClient {
    suspend fun get(url: String): String
}