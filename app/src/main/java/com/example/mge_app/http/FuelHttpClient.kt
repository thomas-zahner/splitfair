package com.example.mge_app.http

import fuel.httpGet

class FuelHttpClient : HttpClient {
    override suspend fun get(url: String): String {
        return url.httpGet().body
    }
}