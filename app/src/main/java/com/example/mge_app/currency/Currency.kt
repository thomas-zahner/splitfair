package com.example.mge_app.currency

import android.os.Parcel
import android.os.Parcelable
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

@Serializable(with = CurrencySerializer::class)
enum class Currency : Parcelable {
    SwissFranc {
        override fun getCode() = "CHF"
    },
    Euro {
        override fun getCode() = "EUR"
    },
    BritishPound {
        override fun getCode() = "GBP"
    },
    UsDollar {
        override fun getCode() = "USD"
    },
    Yen {
        override fun getCode() = "JPY"
    };

    abstract fun getCode(): String

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(getCode())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Currency> {
        override fun createFromParcel(parcel: Parcel): Currency {
            val code = parcel.readString()!!
            return runBlocking { fromCode(code) }
        }

        override fun newArray(size: Int): Array<Currency?> {
            return arrayOfNulls(size)
        }

        suspend fun fromCode(code: String): Currency {
            return Currency.values().asFlow().filter { c -> c.getCode() == code }.firstOrNull()
                ?: throw RuntimeException("Currency with code '$code' not known")
        }
    }
}

object CurrencySerializer : KSerializer<Currency> {
    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor(
            "Currency",
            PrimitiveKind.STRING
        )

    override fun deserialize(decoder: Decoder): Currency {
        return runBlocking { Currency.fromCode(decoder.decodeString()) }
    }

    override fun serialize(encoder: Encoder, value: Currency) {
        encoder.encodeString(value.getCode())
    }
}
