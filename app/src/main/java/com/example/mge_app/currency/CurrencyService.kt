package com.example.mge_app.currency

import com.example.mge_app.http.HttpClient
import fuel.fillURLWithParameters
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

private const val API_KEY = "MlISifAlPNqMqPgWMd52K4p6BrBpEwl9"

class CurrencyService(private val httpClient: HttpClient) {

    private val json = Json { ignoreUnknownKeys = true }

    fun getCurrencies(baseCurrency: Currency): CurrencyConversionRate {
        return runBlocking {
            val url = "https://api.currencybeacon.com/v1/latest".fillURLWithParameters(
                listOf(
                    Pair("api_key", API_KEY),
                    Pair("base", baseCurrency.getCode()),
                    Pair("symbols", getSupportedCurrencySymbols())
                )
            )

            val body = httpClient.get(url)
            json.decodeFromString<LatestResponse>(body).currencyConversionRate
        }
    }

    private suspend fun getSupportedCurrencySymbols(): String {
        return runBlocking {
            Currency.values().asFlow().map { c -> c.getCode() }.toList()
                .joinToString(separator = ",")
        }
    }
}


@Serializable
data class LatestResponse(@SerialName("response") val currencyConversionRate: CurrencyConversionRate)

@Serializable
data class CurrencyConversionRate(val date: String, val base: Currency, val rates: Rates)

typealias Rates = Map<Currency, Double>