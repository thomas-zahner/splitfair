package com.example.mge_app.currency

import com.example.mge_app.data.Expense

class ExpenseConverter(private val conversionRate: CurrencyConversionRate) {
    fun convert(expenses: List<Expense>): List<Expense> {
        return expenses.map { expense ->
            val divisor = conversionRate.rates[expense.currency]!!
            Expense(
                expense.amount / divisor,
                conversionRate.base,
                expense.creator,
                expense.description
            )
        }
    }
}