package com.example.mge_app.ui.activity

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import androidx.activity.ComponentActivity
import com.example.mge_app.R
import com.example.mge_app.currency.Currency
import com.example.mge_app.currency.CurrencyConversionRate
import com.example.mge_app.currency.CurrencyService
import com.example.mge_app.currency.ExpenseConverter
import com.example.mge_app.data.CalculateSettlements
import com.example.mge_app.data.Expense
import com.example.mge_app.data.Group
import com.example.mge_app.data.Transfer
import com.example.mge_app.data.parcelable
import com.example.mge_app.http.FuelHttpClient
import com.example.mge_app.ui.adapter.AdapterHelper


class SettlementActivity : ComponentActivity() {
    private lateinit var group: Group
    private lateinit var currencySpinner: Spinner
    private lateinit var calculateButton: Button
    private lateinit var conversionOverview: TextView
    private lateinit var transferOverview: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settlement_activity)

        group = intent.parcelable("group")!!

        currencySpinner = findViewById(R.id.currencySpinner)
        calculateButton = findViewById(R.id.calculateButton)
        conversionOverview = findViewById(R.id.conversionOverview)
        transferOverview = findViewById(R.id.transferOverview)

        currencySpinner.adapter = AdapterHelper.createCurrencyAdapter(this)

        setupCalculationButton()
    }

    private fun setupCalculationButton() {
        calculateButton.setOnClickListener {
            val currency =
                AdapterHelper.getSpinnerSelection(currencySpinner, Currency.values().toList())!!

            val (convertedExpenses, conversionRate) = convertExpenses(currency)
            val transfers = CalculateSettlements.calculate(group.members.toSet(), convertedExpenses)

            conversionOverview.text = getConversionRateString(conversionRate, currency)
            conversionOverview.visibility = View.VISIBLE

            transferOverview.text = getTransferString(transfers, currency)
            transferOverview.visibility = View.VISIBLE
        }
    }

    private fun convertExpenses(currency: Currency): Pair<List<Expense>, CurrencyConversionRate> {
        val currencyService = CurrencyService(FuelHttpClient())
        val conversionRate = currencyService.getCurrencies(currency)

        val convertedExpenses = ExpenseConverter(conversionRate).convert(group.expenses)
        return Pair(convertedExpenses, conversionRate)
    }

    private fun getConversionRateString(
        conversionRate: CurrencyConversionRate,
        currency: Currency
    ): String {
        return conversionRate.rates
            .filter { it.key != currency }
            .map { rate ->
                val converted = format(rate.value, 3u) + " " + rate.key.getCode()
                "1 " + currency.getCode() + " = " + converted
            }
            .joinToString(separator = "\n")
    }

    private fun getTransferString(transfers: List<Transfer>, currency: Currency): String {
        return transfers.joinToString(separator = "\n") { transfer ->
            val amount = format(transfer.amount)
            "${transfer.from.name} -> ${transfer.to.name}: $amount ${currency.getCode()}"
        }
    }

    private fun format(value: Double, precision: UInt = 2u): String {
        return String.format("%.${precision}f", value)
    }
}
