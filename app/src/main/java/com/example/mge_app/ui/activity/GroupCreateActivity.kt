package com.example.mge_app.ui.activity

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mge_app.R
import com.example.mge_app.data.Group
import com.example.mge_app.data.GroupDao.Companion.save
import com.example.mge_app.data.User
import com.example.mge_app.ui.adapter.UserAdapter
import kotlinx.coroutines.runBlocking

class GroupCreateActivity : ComponentActivity() {
    private var group = Group("", mutableListOf(), mutableListOf())
    private lateinit var userAdapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.group_create_activity)

        setupMemberSection()
        setupSaveButton()
    }

    private fun setupMemberSection() {
        val usersRecyclerView: RecyclerView = findViewById(R.id.usersRecyclerView)
        userAdapter = UserAdapter(group.members)
        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        usersRecyclerView.adapter = userAdapter

        val memberEditText: EditText = findViewById(R.id.memberEditText)
        val addUserButton: Button = findViewById(R.id.addUserButton)

        addUserButton.setOnClickListener {
            val userName = memberEditText.text.toString().trim()
            if (userName.isEmpty()) {
                showToast("Please enter a name")
                return@setOnClickListener
            }

            group.members.add(User(userName))
            userAdapter.notifyItemInserted(group.members.size - 1)
            memberEditText.text.clear()
        }
    }

    private fun setupSaveButton() {
        val groupNameInput: EditText = findViewById(R.id.groupName)
        val saveButton: Button = findViewById(R.id.saveButton)
        val context = this

        saveButton.setOnClickListener {
            group.name = groupNameInput.text.toString().trim()

            if (group.name.isEmpty()) {
                showToast("Please enter a group name")
                return@setOnClickListener
            }

            if (group.members.isEmpty()) {
                showToast("Group must have at least one member")
                return@setOnClickListener
            }

            runBlocking { group = save(group, context)!! }
            finish()
        }
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }
}