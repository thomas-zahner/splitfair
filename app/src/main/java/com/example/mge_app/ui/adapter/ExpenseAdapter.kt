package com.example.mge_app.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mge_app.R
import com.example.mge_app.data.Expense

class ExpenseAdapter(
    private val expenses: List<Expense>,
    private val onExpenseClick: (Int) -> Unit
) :
    RecyclerView.Adapter<ExpenseAdapter.ExpenseViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpenseViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.expense_card, parent, false)
        return ExpenseViewHolder(view)
    }

    override fun onBindViewHolder(holder: ExpenseViewHolder, position: Int) {
        val expense = expenses[position]
        holder.bind(expense)
        holder.itemView.setOnClickListener { onExpenseClick(position) }
    }

    override fun getItemCount(): Int {
        return expenses.size
    }

    class ExpenseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val amountTextView: TextView = itemView.findViewById(R.id.amountTextView)
        private val descriptionTextView: TextView = itemView.findViewById(R.id.descriptionTextView)

        fun bind(expense: Expense) {
            amountTextView.text = formatAmount(expense)
            descriptionTextView.text = formatDescription(expense)
        }

        private fun formatAmount(expense: Expense): String {
            return String.format("%.2f", expense.amount) + " " + expense.currency.getCode()
        }

        private fun formatDescription(expense: Expense): String {
            if (expense.description.isEmpty()) {
                return expense.creator.name
            }
            return expense.creator.name + ": " + expense.description
        }
    }
}