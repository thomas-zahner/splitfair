package com.example.mge_app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.ComponentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mge_app.R
import com.example.mge_app.data.AppDatabase
import com.example.mge_app.data.Group
import com.example.mge_app.ui.adapter.GroupAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.runBlocking

class GroupListActivity : ComponentActivity() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var noGroupsTextView: View
    private lateinit var addGroupButton: FloatingActionButton
    private lateinit var groups: MutableList<Group>
    private lateinit var groupAdapter: GroupAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.group_list_activity)

        recyclerView = findViewById(R.id.recyclerView)
        noGroupsTextView = findViewById(R.id.noGroupsTextView)
        addGroupButton = findViewById(R.id.addGroupButton)

        addGroupButton.setOnClickListener {
            val intent = Intent(this, GroupCreateActivity::class.java)
            this.startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        groups = getGroupsFromDb()
        groupAdapter = GroupAdapter(groups, this)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = groupAdapter
        updateView()
    }

    private fun getGroupsFromDb(): MutableList<Group> {
        val groupDao = AppDatabase.getInstance(this).groupDao()
        val groupEntities = runBlocking { groupDao.getAllGroups() }
        return groupEntities.toMutableList()
    }

    private fun updateView() {
        if (groups.isEmpty()) {
            recyclerView.visibility = View.GONE
            noGroupsTextView.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            noGroupsTextView.visibility = View.GONE
        }
    }
}
