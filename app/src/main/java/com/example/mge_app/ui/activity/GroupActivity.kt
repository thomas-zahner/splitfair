package com.example.mge_app.ui.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mge_app.R
import com.example.mge_app.data.Expense
import com.example.mge_app.data.Group
import com.example.mge_app.data.GroupDao
import com.example.mge_app.data.parcelable
import com.example.mge_app.ui.adapter.ExpenseAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.runBlocking

class GroupActivity : ComponentActivity() {
    private lateinit var expenseView: RecyclerView
    private lateinit var noExpensesTextView: TextView
    private lateinit var addExpenseButton: FloatingActionButton
    private lateinit var calculateSettlements: FloatingActionButton
    private lateinit var group: Group
    private lateinit var expenseAdapter: ExpenseAdapter

    private var createExpenseLauncher = activityResultLauncher()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.group_activity)

        group = intent.parcelable("group")!!
        val groupTitle: TextView = findViewById(R.id.groupTitleTextView)
        groupTitle.text = group.name

        val groupMembers: TextView = findViewById(R.id.groupMembers)
        val members = group.members.joinToString(separator = ", ") { m -> m.name }
        groupMembers.text = getString(R.string.group_members, members)

        expenseView = findViewById(R.id.expenseView)
        noExpensesTextView = findViewById(R.id.noExpensesTextView)
        addExpenseButton = findViewById(R.id.addExpenseButton)
        calculateSettlements = findViewById(R.id.calculateSettlements)
        expenseAdapter = ExpenseAdapter(group.expenses) { position ->
            showDeleteConfirmationDialog(position)
        }

        expenseView.layoutManager = LinearLayoutManager(this)
        expenseView.adapter = expenseAdapter

        setupExpenseButton()
        setupSettlementButton()

        updateView()
    }

    private fun setupExpenseButton() {
        addExpenseButton.setOnClickListener {
            val intent = Intent(this, ExpenseCreateActivity::class.java)
            intent.putExtra("group", group)
            createExpenseLauncher.launch(intent)
        }
    }

    private fun setupSettlementButton() {
        calculateSettlements.setOnClickListener {
            val intent = Intent(this, SettlementActivity::class.java)
            intent.putExtra("group", group)
            this.startActivity(intent)
        }
    }

    private fun activityResultLauncher(): ActivityResultLauncher<Intent> {
        val context = this
        return registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                val expense = result.data!!.parcelable<Expense>("expense")!!
                group.expenses.add(expense)
                expenseAdapter.notifyItemInserted(group.expenses.size - 1)
                runBlocking { GroupDao.save(group, context) }
                updateView()
            }
        }
    }

    private fun showDeleteConfirmationDialog(position: Int) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Confirmation")
        alertDialogBuilder.setMessage("Are you sure you want to delete this expense?")
        alertDialogBuilder.setPositiveButton("Confirm") { _, _ ->
            delete(position)
            updateView()
        }
        alertDialogBuilder.setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
        alertDialogBuilder.create().show()
    }

    private fun delete(position: Int) {
        val context = this
        group.expenses.removeAt(position)
        expenseAdapter.notifyItemRemoved(position)
        runBlocking { GroupDao.save(group, context) }
    }

    private fun updateView() {
        if (group.expenses.isEmpty()) {
            expenseView.visibility = View.GONE
            noExpensesTextView.visibility = View.VISIBLE
        } else {
            expenseView.visibility = View.VISIBLE
            noExpensesTextView.visibility = View.GONE
        }
    }
}

