package com.example.mge_app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.activity.ComponentActivity
import com.example.mge_app.R
import com.example.mge_app.currency.Currency
import com.example.mge_app.data.Expense
import com.example.mge_app.data.Group
import com.example.mge_app.data.parcelable
import com.example.mge_app.ui.adapter.AdapterHelper.Companion.createCurrencyAdapter
import com.example.mge_app.ui.adapter.AdapterHelper.Companion.getSpinnerSelection


class ExpenseCreateActivity : ComponentActivity() {

    private lateinit var group: Group

    private lateinit var amountBeforeCommaText: EditText
    private lateinit var amountAfterCommaText: EditText
    private lateinit var currencySpinner: Spinner
    private lateinit var creatorSpinner: Spinner
    private lateinit var description: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.expense_create_activity)

        group = intent.parcelable("group")!!

        amountBeforeCommaText = findViewById(R.id.amountBeforeComma)
        amountAfterCommaText = findViewById(R.id.amountAfterComma)
        currencySpinner = findViewById(R.id.currencySpinner)
        creatorSpinner = findViewById(R.id.creatorSpinner)
        description = findViewById(R.id.description)

        amountAfterCommaText.setText(getString(R.string.default_value_after_comma))

        setupCurrencySpinner()
        setupCreatorSpinner()

        setupSaveButton()
    }

    private fun setupCreatorSpinner() {
        val groupMembers = group.members.map { it.name }
        val creatorAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, groupMembers)
        creatorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        creatorSpinner.adapter = creatorAdapter
    }

    private fun setupCurrencySpinner() {
        currencySpinner.adapter = createCurrencyAdapter(this)
    }

    private fun setupSaveButton() {
        val saveButton: Button = findViewById(R.id.saveButton)
        saveButton.setOnClickListener {
            val currency = getSpinnerSelection(currencySpinner, Currency.values().toList())
            val creator = getSpinnerSelection(creatorSpinner, group.members)
            val amount = getAmount()
            val description = getDescription()

            if (currency == null || creator == null || amount == null || description.isEmpty()) {
                showToast("Please fill out all fields")
                return@setOnClickListener
            }

            val expense = Expense(amount, currency, creator, description)

            val resultIntent = Intent()
            resultIntent.putExtra("expense", expense)
            setResult(RESULT_OK, resultIntent)
            finish()
        }
    }

    private fun getDescription(): String {
        return this.description.text.toString()
    }

    private fun getAmount(): Double? {
        val beforeCommaText = amountBeforeCommaText.text.toString()
        val afterCommaText = amountAfterCommaText.text.toString()

        if (afterCommaText.length == 1) {
            showToast("Please enter two digits after the comma")
            return null
        }

        val beforeComma = beforeCommaText.toDoubleOrNull()
        val afterComma = afterCommaText.toDoubleOrNull() ?: 0.0

        if (beforeComma == null) {
            return null
        }

        return beforeComma + (afterComma / 100.0)
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }
}
