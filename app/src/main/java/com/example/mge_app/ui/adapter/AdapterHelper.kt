package com.example.mge_app.ui.adapter

import android.content.Context
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import com.example.mge_app.currency.Currency

class AdapterHelper {
    companion object {
        fun createCurrencyAdapter(context: Context): ArrayAdapter<String> {
            val currencyNames = Currency.values().map { it.name }
            val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, currencyNames)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            return adapter
        }

        fun <T> getSpinnerSelection(spinner: Spinner, items: List<T>): T? {
            val selectedPosition = spinner.selectedItemPosition
            return if (selectedPosition != AdapterView.INVALID_POSITION) {
                items[selectedPosition]
            } else {
                null
            }
        }

    }
}