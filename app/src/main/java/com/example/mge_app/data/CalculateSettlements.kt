package com.example.mge_app.data

class CalculateSettlements {
    companion object {
        fun calculate(members: Set<User>, expenses: List<Expense>): List<Transfer> {
            assertExpensesAreMadeByMembers(members, expenses)
            assertSingleCurrency(expenses)
            if (members.isEmpty() || expenses.isEmpty()) return emptyList()

            val totalAmount = expenses.sumOf { it.amount }
            val targetAmountPerUser = totalAmount / members.size

            val debts = mutableMapOf<User, Double>()
            val transfers = mutableListOf<Transfer>()

            for (member in members) {
                val expensesOfMember = expenses.filter { it.creator == member }
                val expenseSumOfMember = expensesOfMember.sumOf { it.amount }
                debts[member] = (targetAmountPerUser - expenseSumOfMember)
            }

            for (payer in debts.keys) {
                for (recipient in debts.keys) {
                    if (payer == recipient) continue
                    if (debts[payer]!! <= 0) break // if payer has no debts (anymore)
                    if (debts[recipient]!! >= 0) continue // if recipient has debts

                    val transferAmount = minOf(debts[payer]!!, -debts[recipient]!!)

                    debts[payer] = debts[payer]!! - transferAmount
                    debts[recipient] = debts[recipient]!! + transferAmount

                    transfers.add(Transfer(from = payer, to = recipient, transferAmount))
                }
            }

            return transfers
        }

        private fun assertSingleCurrency(expenses: List<Expense>) {
            val currencies = expenses.map { expense -> expense.currency }.toSet()
            if (currencies.size > 1) {
                throw IllegalArgumentException("Expenses must be in all in the same currency. Found: $currencies")
            }
        }

        private fun assertExpensesAreMadeByMembers(members: Set<User>, expenses: List<Expense>) {
            for (expense in expenses) {
                if (!members.contains(expense.creator)) {
                    throw IllegalArgumentException(expense.creator.name + " is not a member")
                }
            }
        }
    }
}

data class Transfer(val from: User, val to: User, val amount: Double)