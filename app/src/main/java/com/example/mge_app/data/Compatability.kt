package com.example.mge_app.data

import android.content.Intent
import android.os.Build.VERSION.SDK_INT
import android.os.Parcelable

/*
    Source: https://stackoverflow.com/questions/73019160/the-getparcelableextra-method-is-deprecated
 */

inline fun <reified T : Parcelable> Intent.parcelable(key: String): T? = when {
    SDK_INT >= 33 -> getParcelableExtra(key, T::class.java)
    else -> @Suppress("DEPRECATION") getParcelableExtra(key) as? T
}