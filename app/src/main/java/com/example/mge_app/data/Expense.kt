package com.example.mge_app.data


import android.os.Parcel
import android.os.Parcelable
import com.example.mge_app.currency.Currency
import kotlinx.serialization.Serializable

@Serializable
data class Expense(
    val amount: Double,
    val currency: Currency,
    val creator: User,
    val description: String = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        Currency.createFromParcel(parcel),
        User.createFromParcel(parcel),
        parcel.readString()!!
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(amount)
        currency.writeToParcel(parcel, flags)
        creator.writeToParcel(parcel, flags)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Expense> {
        override fun createFromParcel(parcel: Parcel): Expense {
            return Expense(parcel)
        }

        override fun newArray(size: Int): Array<Expense?> {
            return arrayOfNulls(size)
        }
    }
}

