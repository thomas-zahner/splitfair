package com.example.mge_app.data

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

@Entity(tableName = "groups")
data class Group(
    var name: String,
    @field:TypeConverters(Converter::class) val members: MutableList<User>,
    @field:TypeConverters(Converter::class) val expenses: MutableList<Expense>,
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        mutableListOf<User>().apply { parcel.readTypedList(this, User.CREATOR) },
        mutableListOf<Expense>().apply { parcel.readTypedList(this, Expense.CREATOR) },
        parcel.readLong(),
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeTypedList(members)
        parcel.writeTypedList(expenses)
        parcel.writeLong(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Group> {
        override fun createFromParcel(parcel: Parcel): Group {
            return Group(parcel)
        }

        override fun newArray(size: Int): Array<Group?> {
            return arrayOfNulls(size)
        }
    }
}