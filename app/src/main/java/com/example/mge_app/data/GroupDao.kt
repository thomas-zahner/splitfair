package com.example.mge_app.data

import android.content.Context
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface GroupDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGroup(group: Group): Long

    @Query("SELECT * FROM groups WHERE id = :groupId")
    suspend fun getGroupById(groupId: Long): Group?

    @Query("SELECT * FROM groups")
    suspend fun getAllGroups(): List<Group>

    companion object {
        suspend fun save(group: Group, context: Context): Group? {
            val groupDao = AppDatabase.getInstance(context).groupDao()
            val id = groupDao.insertGroup(group)
            return groupDao.getGroupById(id)
        }
    }
}
