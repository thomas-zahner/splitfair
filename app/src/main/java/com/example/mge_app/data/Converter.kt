package com.example.mge_app.data

import androidx.room.TypeConverter
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class Converter {
    @TypeConverter
    fun fromUserList(value: MutableList<User>) = Json.encodeToString(value)

    @TypeConverter
    fun toUserList(value: String) = Json.decodeFromString<MutableList<User>>(value)

    @TypeConverter
    fun fromExpenseList(value: MutableList<Expense>) = Json.encodeToString(value)

    @TypeConverter
    fun toExpenseList(value: String) = Json.decodeFromString<MutableList<Expense>>(value)
}