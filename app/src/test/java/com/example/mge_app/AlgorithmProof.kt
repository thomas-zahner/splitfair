package com.example.mge_app

import com.example.mge_app.currency.Currency
import com.example.mge_app.data.CalculateSettlements
import com.example.mge_app.data.Expense
import com.example.mge_app.data.Transfer
import com.example.mge_app.data.User
import org.junit.Test
import java.util.UUID
import kotlin.math.absoluteValue
import kotlin.random.Random

const val TOLERANCE_MARGIN: Double = 0.000_000_1
const val MAX_EXPENSE_AMOUNT = 200.0
val EXPENSE_COUNT_PER_USER = 0..10
const val USER_COUNT = 100

class AlgorithmProof {
    @Test
    fun randomGenerativeProof() {
        val (users, accounts) = generateAccounts()
        val accountingSystem = AccountingSystem(accounts)

        val expenses = generateExpenses(accounts)
        accountingSystem.addExpenses(expenses)

        val transfers = CalculateSettlements.calculate(users, expenses)
        accountingSystem.handleTransfers(transfers)

        assert(accountingSystem.isBalanceSettled())
    }

    private fun generateAccounts(): Pair<Set<User>, Set<Account>> {
        val users = (0 until USER_COUNT).map { User(UUID.randomUUID().toString()) }
        return Pair(users.toSet(), users.map { Account(it) }.toSet())
    }

    private fun generateExpenses(accounts: Set<Account>): List<Expense> {
        return accounts.flatMap { account ->
            val expenseCount = Random.nextInt(
                EXPENSE_COUNT_PER_USER.first,
                EXPENSE_COUNT_PER_USER.last + 1
            )
            (0 until expenseCount).map {
                Expense(
                    Random.nextDouble() * MAX_EXPENSE_AMOUNT,
                    Currency.SwissFranc,
                    account.user
                )
            }
        }
    }
}

class AccountingSystem(private val accounts: Set<Account>) {
    fun addExpenses(expenses: List<Expense>) {
        for (expense in expenses) {
            val account = getAccount(expense.creator)
            account.balance -= expense.amount
        }
    }

    fun handleTransfers(transfers: List<Transfer>) {
        for (transfer in transfers) {
            getAccount(transfer.from).balance -= transfer.amount
            getAccount(transfer.to).balance += transfer.amount
        }
    }

    fun isBalanceSettled(): Boolean {
        val balances = accounts.map { it.balance }
        val averageBalance = balances.average()

        return balances.map { balance ->
            val delta = balance - averageBalance
            delta.absoluteValue < TOLERANCE_MARGIN
        }.all { it }
    }

    private fun getAccount(user: User) = accounts.find { it.user == user }!!
}

data class Account(val user: User, var balance: Double = 0.0)
