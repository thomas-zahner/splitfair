package com.example.mge_app

import com.example.mge_app.data.User
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class UserTest {
    @Test
    fun equality() {
        assertEquals(User("Alice"), User("Alice"))
        assertEquals(User("Bob"), User("Bob"))
    }

    @Test
    fun inequality() {
        assertNotEquals(User("Alice"), User("Bob"))
        assertNotEquals(User("Bob"), User("Alice"))
    }

    @Test
    fun setOfUsers() {
        val userSet = setOf(User("Alice"), User("Alice"), User("Bob"))
        assertEquals(2, userSet.size)
    }
}