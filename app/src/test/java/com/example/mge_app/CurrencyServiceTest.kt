package com.example.mge_app

import com.example.mge_app.currency.Currency
import com.example.mge_app.currency.CurrencyService
import com.example.mge_app.http.FuelHttpClient
import com.example.mge_app.http.HttpClientMock
import org.junit.Assert.assertEquals
import org.junit.Test

class CurrencyServiceTest {
    @Test
    fun currencyServiceWorks() {
        val service = CurrencyService(HttpClientMock.MockClient)
        val currencies = service.getCurrencies(Currency.UsDollar)
        assertEquals(currencies.rates[Currency.UsDollar], 1.0)
        assertEquals(currencies.rates[Currency.SwissFranc], 0.91683702)
    }

    @Test
    fun fuelClientAndCurrencyServiceWork() {
        val service = CurrencyService(FuelHttpClient())
        val currencies = service.getCurrencies(Currency.UsDollar)
        assertEquals(currencies.rates[Currency.UsDollar], 1.0)
    }
}

