package com.example.mge_app

import com.example.mge_app.currency.Currency
import com.example.mge_app.data.Converter
import com.example.mge_app.data.Expense
import com.example.mge_app.data.User
import org.junit.Assert.assertEquals
import org.junit.Test

class ConverterTest {
    private val converter = Converter()
    private val userJson = """[{"name":"name"}]"""
    private val users = mutableListOf(User("name"))
    private val expenses = mutableListOf(Expense(123.0, Currency.Euro, users[0]))
    private val expenseJson = """[{"amount":123.0,"currency":"EUR","creator":{"name":"name"}}]"""

    @Test
    fun fromUserListWorks() {
        assertEquals(userJson, converter.fromUserList(users))
    }

    @Test
    fun toUserListWorks() {
        assertEquals(users, converter.toUserList(userJson))
    }

    @Test
    fun fromExpenseList() {
        assertEquals(expenseJson, converter.fromExpenseList(expenses))
    }

    @Test
    fun toExpenseList() {
        assertEquals(expenses, converter.toExpenseList(expenseJson))
    }
}
