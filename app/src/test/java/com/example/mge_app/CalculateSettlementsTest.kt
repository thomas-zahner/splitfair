package com.example.mge_app

import com.example.mge_app.currency.Currency
import com.example.mge_app.data.CalculateSettlements.Companion.calculate
import com.example.mge_app.data.Expense
import com.example.mge_app.data.Transfer
import com.example.mge_app.data.User
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Test

class CalculateSettlementsTest {
    private val alice = User("Alice")
    private val bob = User("Bob")
    private val charlie = User("Charlie")

    @Test
    fun noMembersAndNoExpenses() {
        val transfers = calculate(emptySet(), emptyList())
        assert(transfers.isEmpty())
    }

    @Test
    fun noExpenses() {
        val transfers = calculate(setOf(alice, bob), emptyList())
        assert(transfers.isEmpty())
    }

    @Test
    fun expensesFromNonMembers() {
        val members = setOf(alice)
        val expenses = listOf(Expense(1.0, Currency.Euro, bob))
        val exception =
            assertThrows(IllegalArgumentException::class.java) { calculate(members, expenses) }
        assertEquals("Bob is not a member", exception.message)
    }

    @Test
    fun mixedCurrencies() {
        val members = setOf(alice)
        val expenses = listOf(
            Expense(1.0, Currency.SwissFranc, alice),
            Expense(1.0, Currency.Euro, alice)
        )

        val exception =
            assertThrows(IllegalArgumentException::class.java) { calculate(members, expenses) }
        assertEquals(
            "Expenses must be in all in the same currency. Found: [SwissFranc, Euro]",
            exception.message
        )
    }

    @Test
    fun normalUseCase() {
        val members = setOf(alice, bob, charlie)
        val expenses = listOf(
            Expense(20.0, Currency.Euro, alice),
            Expense(30.0, Currency.Euro, bob),
            Expense(10.0, Currency.Euro, charlie)
        )

        val transfers = calculate(members, expenses)

        assertEquals(
            listOf(
                Transfer(charlie, bob, 10.0)
            ), transfers
        )
    }

    @Test
    fun notPerfectlyDivisibleAmount() {
        val members = setOf(alice, bob, charlie)
        val expenses = listOf(Expense(100.0, Currency.Euro, alice))

        val transfers = calculate(members, expenses)
        assertEquals(
            listOf(
                Transfer(bob, alice, 33.333333333333336),
                Transfer(charlie, alice, 33.33333333333332)
            ), transfers
        )
    }
}