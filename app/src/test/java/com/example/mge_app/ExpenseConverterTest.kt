package com.example.mge_app

import com.example.mge_app.currency.Currency
import com.example.mge_app.currency.CurrencyService
import com.example.mge_app.currency.ExpenseConverter
import com.example.mge_app.data.Expense
import com.example.mge_app.data.User
import com.example.mge_app.http.HttpClientMock
import org.junit.Assert.assertEquals
import org.junit.Test

class ExpenseConverterTest {
    private val alice = User("Alice")
    private val bob = User("Bob")

    private val service = CurrencyService(HttpClientMock.MockClient)
    private val conversionRate = service.getCurrencies(Currency.UsDollar)
    private val converter = ExpenseConverter(conversionRate)

    @Test
    fun conversion() {
        val expenses = listOf(
            Expense(148.86174965, Currency.Yen, alice),
            Expense(0.91683702, Currency.SwissFranc, bob)
        )
        val converted = converter.convert(expenses)
        assertEquals(
            listOf(
                Expense(1.0, Currency.UsDollar, alice),
                Expense(1.0, Currency.UsDollar, bob)
            ), converted
        )
    }
}