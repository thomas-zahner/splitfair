# Splitfair

When going on a trip with friends, it can be cumbersome to split expenses individually. Splitfair
simplifies this process. You can create a group for your trip, add your friends, and record all
the expenses. Then, with just one click, the app calculates and shows you how much each
person owes or is owed within the group.

## Install

Get the latest installable APK of splitfair on the [release page](https://gitlab.com/thomas-zahner/splitfair/-/releases).

## Functionality

### Group overview

A list of all available groups will be on display at the start of the application.
The floating action button in the bottom right can be used to create a new group.

![Group overview](docs/group-list.png)

### Create group

To create a new group a name and the participants must be specified.
At least one participant is required to create a group.
When the group is created the user is taken back to the group overview screen
where the new group will show up.

![Create a new group](docs/create-group.png)

### Group screen

Clicking on a group in the overview screen leads to the group screen.
In this screen the details of the group are shown.
The plus button allows the creation of new expenses of the group.
The button with the dollar sign will lead to the settlement screen.
Simply click on an expense if you want to delete it.

![Create a new group](docs/group-without-expenses.png)

### Expense creation screen

When creating a new expense the amount must be specified.
Additionally, the currency and the person who spent the money must be specified.

![](docs/create-expense.png)

### Settlement screen

Once the expenses are recorded, the settlement can be calculated.
To do this, select the currency in which you would like to view the settlement and click '
Calculate'. The exchange rates are displayed first. After that, the settlements are displayed.

![](docs/settlement.png)

In this example both Alice and Bob owe Charlie 20.51 swiss francs.
Once they have given Charlie the money, everyone in the group will have spent the same amount of
money in total.

## Points

- Using Kotlin instead of Java (3 points)
- Functionality (4 points)
- Use web service (1 point)
- Persistence (2 points)
